"""
Manage Bliss data saving (SCAN_SAVING)
"""

import os
import re
from datetime import datetime
from collections import OrderedDict
from numbers import Integral
from ..io import utils as io_utils
from ..utils import config
from . import naming





def newexperiment(proposal, type=None, bl=None,
                  root=None, verify=True):
    """
    Change experiment in session's SCAN_SAVING object

    :param str proposal: e.g. 'hg94'
    :param str type: 'inhouse', 'visitor', 'default' or None ('/tmp')
    :param str bl: beamline
    :param str root: proposal root directory
    :param bool verify: verify valid proposal name
    """
    scan_saving = config.scan_saving()
    if not bl:
        try:
            bl = scan_saving.beamline
        except AttributeError:
            raise ValueError('Beamline name is required')
    if not proposal and type == 'inhouse':
        type = 'default'
    if proposal:
        proposal = naming.proposal_name(proposal)
    if bl:
        bl = naming.beamline_name(bl)

    # Root directory of proposals:
    if type == 'inhouse':
        now = datetime.now().strftime('%y%b').lower()
        root = os.path.join(os.sep, 'data', bl, scan_saving.inhouse_folder, now)
    elif type == 'visitor':
        if not proposal:
            raise ValueError('Proposal name is required')
        root = os.path.join(os.sep, 'data', 'visitor')
        if verify:
            if not os.path.isdir(os.path.join(root, proposal)):
                raise ValueError('Proposal {} does not exist'
                                 .format(repr(proposal)))
    elif type == 'default':
        root = os.path.join(os.sep, 'data', bl, scan_saving.inhouse_folder, 'default')
        proposal = datetime.now().strftime(bl + '%y%m')
    else:
        if not root:
            root = os.path.join(os.sep, 'data', bl, 'tmp')
            if not root:
                root = io_utils.temproot()  # /tmp
        if not proposal:
            # Random non-existent proposal
            chars = 'abcdefghijklmnopqrstuvwxyz0123456789'
            proposal = io_utils.tempname(prefix='proposal', chars=chars)
            while os.path.exists(os.path.join(root, proposal)):
                proposal = io_utils.tempname(prefix='proposal', chars=chars)

    # Define template for dataset root `scan_saving.get_path()`
    scan_saving.template = naming.static_template()
    scan_saving.base_path = root
    naming.raiseIfInvalid(experiment=proposal)
    scan_saving.add('experiment', proposal)
    if bl:
        naming.raiseIfInvalid(beamline=bl)
        scan_saving.add('beamline', bl)


def newvisitor(proposal, **kwargs):
    """
    Switch to user experiment in session's SCAN_SAVING object

    :param str proposal: e.g. 'hg94'
    :param **kwargs: see `newexperiment`
    """
    kwargs['type'] = 'visitor'
    newexperiment(proposal, **kwargs)


def newinhouse(proposal, **kwargs):
    """
    Switch to inhouse experiment in session's SCAN_SAVING object

    :param str proposal: e.g. 'blc10696' (default beamline session
                         of the month when `None`)
    :param **kwargs: see `newexperiment`
    """
    kwargs['type'] = 'inhouse'
    newexperiment(proposal, **kwargs)


def assert_experiment(scan_saving):
    try:
        for attr in 'experiment', 'beamline':
            getattr(scan_saving, attr)
    except AttributeError:
        raise RuntimeError("Run 'newexperiment' first")


def assert_sample(scan_saving):
    assert_experiment(scan_saving)
    try:
        getattr(scan_saving, 'sample')
    except AttributeError:
        raise RuntimeError("Run 'newsample' first")


def assert_dataset(scan_saving):
    assert_sample(scan_saving)
    try:
        for attr in 'dataset', 'technique':
            getattr(scan_saving, attr)
    except AttributeError:
        raise RuntimeError("Run 'newdataset' first")


def newsample(name=None):
    """
    Change sample in session's SCAN_SAVING object

    :param str name: keep current by default ('default' when missing)
    """
    scan_saving = config.scan_saving()
    assert_experiment(scan_saving)
    try:
        scan_saving.sample
    except AttributeError:
        if not name:
            name = 'default'
        naming.raiseIfInvalid(sample=name)
        scan_saving.add('sample', name)
    else:
        if name:
            naming.raiseIfInvalid(sample=name)
            scan_saving.sample = name
        elif not scan_saving.sample:
            scan_saving.sample = 'default'


def newdataset(name=None, sampleprefix=False, nonexisting=False):
    """
    Change dataset in session's SCAN_SAVING object.
    When no name is specify, a new number starting from 1 will be used

    :param str or num name:
    :param bool sampleprefix: sample name in dataset name
    :param bool nonexisting: nonexisting dataset directory
    """
    scan_saving = config.scan_saving()
    assert_sample(scan_saving)
    template = os.path.join('{base_path}', scan_saving.template)
    variables = config.scan_saving_pathinfo()
    name = naming.dataset_name(name, template, variables,
                               sampleprefix=sampleprefix,
                               nonexisting=nonexisting)
    scan_saving.add('dataset', name)


def switchtechnique(name=None):
    """
    Change technique in session's SCAN_SAVING object

    :param str name:
    """
    scan_saving = config.scan_saving()
    assert_dataset(scan_saving)
    if not name:
        name = config.scan_saving_get('technique')
    if not name:
        name = config.default_technique()
    lst = config.techniques()
    if lst and name not in lst:
        raise ValueError('{} is not a valid technique. Available techniques are {}'.format(repr(name), lst))
    scan_saving.add('technique', name)


def saveinfo():
    """
    Bliss saving info

    :returns OrderedDict:
    """
    scan_saving = config.scan_saving()
    info = OrderedDict()
    try:
        scan_saving.get_path()
    except RuntimeError:
        info['status'] = 'INCOMPLETE'
    else:
        info['status'] = 'COMPLETE'
    info['path'] = scan_saving.get_path()

    attrs = ['template', 'experiment', 'base_path',
             'beamline', 'sample', 'dataset', 'technique']
    keymap = {'base_path': 'root', 'experiment': 'proposal'}
    for attr in attrs:
        try:
            info[keymap.get(attr, attr)] = getattr(scan_saving, attr, None)
        except RuntimeError:
            info[keymap.get(attr, attr)] = None
    return info
