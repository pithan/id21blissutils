"""
Ensure proper data naming
"""
import os
import re
import datetime
from glob import glob


def static_template():
    """
    Template for directory structure

    :returns str:
    """
    return os.path.join('{experiment}',
                        '{beamline}',
                        '{sample}',
                        '{dataset}')


def static_filename_templates():
    """
    Templates for HDF5 file names, starting from the dataset
    filename as defined in `static_template`, followed by
    the master filenames in upper directories.

    :returns list(str):
    """
    return ['{dataset}.h5',
            '{experiment}_{sample}.h5',
            '{experiment}_{beamline}.h5']


def timestamp():
    return datetime.datetime.now().strftime('%Y_%m_%d_%H%M%S')


def nextpath(pathgen, name=None, ndigits=None, nonexisting=True):
    """
    Find the next path (increment when number or suffix when non-existing)

    :param callable pathgen: takes string argument
    :param str or num name:
    :param bool nonexisting: path must be non-existent
    :returns name(str), fmt(str): pathgen(fmt.format(name))
    """
    if not name:
        name = 1
    try:
        isnumber = name.isdigit()
        if isnumber:
            ndigits = len(name)
            name = int(name)
    except AttributeError:
        isnumber = True
    if ndigits is None:
        ndigits = 4

    if nonexisting:
        if isnumber:
            # not lower than the maximal existing number + 1
            paths = glob(pathgen('*'))
            pattern = re.compile(pathgen('([0-9]+)'))
            numbers = [pattern.match(p) for p in paths]
            numbers = [int(m.group(1)) for m in numbers if m]
            if numbers:
                name = max(name, max(numbers)+1)
            inputfmt = '{{:0{:d}d}}'.format(ndigits)
        elif os.path.exists(pathgen(name)):
            paths = glob(pathgen(name + '_*'))
            pattern = re.compile(pathgen(name + '_([0-9]+)'))
            numbers = [pattern.match(p) for p in paths]
            numbers = [int(m.group(1)) for m in numbers if m]
            if numbers:
                num = max(numbers)+1
            else:
                num = 1
            fmt = '{}_{{:0{:d}d}}'.format(name, ndigits)
            name = fmt.format(num)
            inputfmt = '{}'
        else:
            inputfmt = '{}'
    else:
        if isnumber:
            inputfmt = '{{:0{:d}d}}'.format(ndigits)
        else:
            inputfmt = '{}'
    return name, inputfmt


def sample_in_dataset_name(template, dataset, sample):
    """
    Sample must be in the last occurance of dataset

    :param str template: absolute path template
    :param str dataset: template variable name
    :param str sample: template variable name
    :returns str:
    """
    datasetfmt = '{{{}}}'.format(dataset)
    samplefmt = '{{{}}}'.format(sample)
    namefmt = datasetfmt
    for p in reversed(os.path.split(template)):
        if datasetfmt in p:
            if samplefmt not in p:
                namefmt = samplefmt + '_' + datasetfmt
            break
    return namefmt


def raiseIfInvalid(_pattern=r'^[0-9a-zA-Z_]+$', **kwargs):
    """
    :param str _pattern:
    :param **kwargs: variables to check
    :raises ValueError:
    """
    pattern = re.compile(_pattern)
    for k, v in kwargs.items():
        if not pattern.match(v):
            raise ValueError('{} contains invalid characters (only [0-9a-zA-Z_] allowed)'
                             .format(repr(k)))


def proposal_name(proposal):
    """
    :param str proposal:
    :returns str:
    """
    raiseIfInvalid(proposal=proposal)
    # Current characters: 0-9a-zA-Z_
    proposal = proposal.lower()
    proposal = proposal.replace('_', '')
    return proposal


def beamline_name(bl):
    """
    :param str bl:
    :returns str:
    """
    raiseIfInvalid(_pattern=r'^[0-9a-zA-Z]+$', bl=bl)
    # Current characters: 0-9a-zA-Z
    return bl.lower()


def dataset_name(name, template, variables, sampleprefix=False,
                 nonexisting=True, validate=True,
                 meta=False, ndigits=None):
    """
    :param str or num name: proposed name
    :param str template: absolute path template
    :param dict variables: values for `template`
    :param bool sampleprefix: sample name must be in
                              dataset name
    :param bool nonexisting: path must not exist
    :param bool validate: valid dataset name
    :param bool meta: BLiss or MetadataManager template
    :param int ndigits:
    :returns str:
    """
    if meta:
        dataset = 'scanName'
        sample = 'sampleName'
    else:
        dataset = 'dataset'
        sample = 'sample'
    variables = dict(variables)
    # Make sure sample name in dataset name:
    dnamefmt = '{{{}}}'.format(dataset)
    if sampleprefix:
        samplefmt = '{{{}}}'.format(sample)
        samplename = samplefmt.format(**variables)
        try:
            templateprefix = samplename not in name
        except TypeError:
            templateprefix = True
        if templateprefix:
            dnamefmt = sample_in_dataset_name(template, dataset, sample)
    variables[dataset] = '{{{}}}'.format(dataset)
    # Template for path and dataset with {dataset}: for example
    # pathfmt = '/tmp/tmpwtmtj_fx/id210000/id21/default/{dataset}'
    # namefmt = 'default_{dataset}'
    pathfmt = template.format(**variables)
    namefmt = dnamefmt.format(**variables)

    def templatekwargsgen(v):
        return {dataset: v}

    def pathgen(v):
        v = namefmt.format(**templatekwargsgen(v))
        return pathfmt.format(**templatekwargsgen(v))
    newname, fmt = nextpath(pathgen, name=name, nonexisting=nonexisting,
                            ndigits=ndigits)
    newname = namefmt.format(**templatekwargsgen(fmt.format(newname)))
    if validate:
        raiseIfInvalid(**templatekwargsgen(newname))
    return newname
