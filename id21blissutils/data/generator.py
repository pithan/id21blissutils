"""
Define generators for scan data publishing in Redis
"""
import inspect
import enum
import logging
import os
from bliss import global_map
from bliss.scanning import scan_meta
from bliss.common.measurement import SamplingMode
from ..utils import config


logger = logging.getLogger(__name__)


def initialize(force=False):
    """
    Initialize the scan metadata generators

    :param bool force: re-initialize when already done
    """
    # Add custom categories
    try:
        generators = scan_meta.get_user_scan_meta()
        for attr in ['external']:
            getattr(generators, attr)
        if force:
            raise AttributeError
    except AttributeError:
        cats = [m.name for m in scan_meta.CATEGORIES] + ['EXTERNAL']
        scan_meta.CATEGORIES = enum.Enum(scan_meta.CATEGORIES.__name__, list(set(cats)))
        scan_meta.USER_SCAN_META = None
        create()


def create():
    """
    Create the scan metadata generators
    """
    # Generators are called at the start of the scan:
    #   bliss.scanning.scan.Scan.__init__
    # and at the end of the scan
    #   run bliss.scanning.scan.Scan.run (cleanup section)
    #
    # The generator 'instrument.positioners' is an exception.
    # It is only called at the beginning of the scan by
    # removing it before calling the generators a second time.
    logger.debug('Create scan metadata generators')
    generators = scan_meta.get_user_scan_meta()
    generators.clear()
    instrument = generators.instrument
    instrument.set('positioners', fill_positioners)  # start of scan
    external = generators.external
    external.set('instrument', fill_instrument_name)
    external.set('positioners', fill_positioners)  # end of scan
    external.set('device_info', fill_device_info)
    external.set('technique', fill_technique_info)
    external.set('filenames', fill_filenames)


def fill_positioners(scan):
    """
    :param bliss.scanning.scan.Scan scan:
    """
    logger.debug('fill motor positions')
    data = {}
    data['positioners'] = positions = {}
    data['positioners_dial'] = dials = {}
    data['positioners_units'] = units = {}
    for name, pos, dial, unit in global_map.get_axes_positions_iter(on_error='ERR'):
        positions[name] = pos
        dials[name] = dial
        units[name] = unit
    return data


def fill_instrument_name(scan):
    """
    :param bliss.scanning.scan.Scan scan:
    """
    logger.debug('fill instrument name')
    root = config.static_config_root()
    name = root.get('synchrotron', '')
    beamline = root.get('beamline', '')
    beamline = config.scan_saving_get('beamline', beamline)
    if beamline:
        if name:
            name += ': ' + beamline
        else:
            name = beamline
    return {'instrument': name}


def fill_technique_info(scan):
    """
    :param bliss.scanning.scan.Scan scan:
    """
    logger.debug('fill technique info')
    return {'technique': config.current_technique_definition()}


def fill_device_info(scan):
    """
    :param bliss.scanning.scan.Scan scan:
    """
    logger.debug('fill device info')
    return {'devices': device_redis_info(scan)}


def fill_filenames(scan):
    """
    :param bliss.scanning.scan.Scan scan:
    """
    logger.debug('fill filename info')
    filenames = [name for name in config.filenames() if name]
    return {'filenames': filenames}


def device_redis_info(scan):
    """
    Publish information on devices (defines types and groups counters).

    :param bliss.scanning.scan.Scan scan:
    :returns dict:
    """
    devices = {}
    # This is not all of them
    for ctr in global_map.get_counters_iter():
        fullname = ctr.fullname.replace('.', ':')  # Redis name
        # Derived from: bliss.common.measurement.BaseCounter
        #   bliss.common.measurement.Counter
        #       bliss.common.measurement.SamplingCounter
        #           bliss.common.temperature.TempControllerCounter
        #           bliss.controllers.simulation_diode.SimulationDiodeSamplingCounter
        #   bliss.scanning.acquisition.mca.BaseMcaCounter
        #       bliss.scanning.acquisition.mca.SpectrumMcaCounter
        #       bliss.scanning.acquisition.mca.StatisticsMcaCounter
        ctr_classes = [c.__name__ for c in inspect.getmro(ctr.__class__)]
        #print(ctr.fullname, type(ctr), type(ctr.controller), ctr_classes)
        #controller_classes = [c.__name__ for c in inspect.getmro(ctr.controller.__class__)]
        if 'SpectrumMcaCounter' in ctr_classes:
            device_info = {'type': 'mca',
                           'description': ctr.controller.detector_brand +\
                                          '/' + ctr.controller.detector_type}
            device = {'device_info': device_info,
                      'device_type': 'mca'}
            devices[fullname] = device
        elif 'StatisticsMcaCounter' in ctr_classes:
            device_info = {'type': 'mca',
                           'description': ctr.controller.detector_brand +\
                                          '/' + ctr.controller.detector_type}
            device = {'device_info': device_info,
                      'device_type': 'mca'}
            devices[fullname] = device
        elif 'RoiMcaCounter' in ctr_classes:
            device_info = {'type': 'mca',
                           'description': ctr.mca.detector_brand +\
                                          '/' + ctr.mca.detector_type}
            roi = ctr.mca.rois.resolve(ctr.roi_name)
            data_info = {'roi_start': roi[0],
                         'roi_end': roi[1]}
            device = {'device_info': device_info,
                      'data_info': data_info,
                      'device_type': 'mca'}
            devices[fullname] = device
        elif 'LimaBpmCounter' in ctr_classes:
            device_info = {'type': 'lima'}
            device = {'device_info': device_info,
                      'device_type': 'lima'}
            devices[fullname] = device
        elif 'LimaImageCounter' in ctr_classes:
            device_info = {'type': 'lima'}
            device = {'device_info': device_info,
                      'device_type': 'lima'}
            devices[fullname] = device
        elif 'RoiStatCounter' in ctr_classes:
            device_info = {'type': 'lima'}
            roi = ctr.parent_roi_counters.get(ctr.roi_name)
            data_info = {'roi_'+k: v for k, v in roi.to_dict().items()}
            device = {'device_info': device_info,
                      'device_type': 'lima',
                      'data_info': data_info}
            devices[fullname] = device
        elif 'TempControllerCounter' in ctr_classes:
            device_info = {'type': 'temperature',
                           'description': 'temperature'}
            device = {'device_info': device_info,
                      'device_type': 'temperature'}
            devices[fullname] = device
        elif 'SamplingCounter' in ctr_classes:
            device_info = {'type': 'samplingcounter',
                           'mode': str(ctr.mode).split('.')[-1]}
            device = {'device_info': device_info,
                      'device_type': 'samplingcounter',
                      'data_type': 'signal'}
            devices[fullname] = device
            if ctr.mode == SamplingMode.SAMPLES:
                device = {'device_info': device_info,
                          'device_type': 'samplingcounter'}
                devices[fullname+'_samples'] = device
            elif ctr.mode == SamplingMode.STATS:
                for stat in 'N', 'std', 'var', 'min', 'max', 'p2v':
                    device = {'device_info': device_info,
                              'device_type': 'samplingcounter'}
                    devices[fullname+'_'+stat] = device
        else:
            logger.info('Counter {} {} published as generic detector'
                        .format(fullname, ctr_classes))
            devices[fullname] = {}
    return devices
