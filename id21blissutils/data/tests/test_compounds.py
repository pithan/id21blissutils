import unittest
from .. import compounds


class test_compounds(unittest.TestCase):

    def test_formula(self):
        parser = compounds.FormulaParser()
        elements = {'Co': 3, 'Fe': 2, 'C': 2, 'N': 12}
        self.assertEqual(parser.parse('Co3(Fe(CN6))2'), elements)
        self.assertTrue(parser.validate('Co3(Fe(CN6))2'))
        self.assertFalse(parser.validate('Co3(Fe(CN6)2'))


def test_suite():
    """Test suite including all test suites"""
    testSuite = unittest.TestSuite()
    testSuite.addTest(test_compounds('test_formula'))
    return testSuite


if __name__ == '__main__':
    import sys

    mysuite = test_suite()
    runner = unittest.TextTestRunner()
    if not runner.run(mysuite).wasSuccessful():
        sys.exit(1)
