import unittest
import os
import re
from ...testing.unittest import BlissTestCase
from .. import blissdata
from ...io.utils import mkdir


anydir = '[^{}]+'.format(os.sep)


class test_blissdata(BlissTestCase):

    def setUp(self):
        super(test_blissdata, self).setUp(xrd=False, fullfield=False)

    @property
    def save_info(self):
        return self.session_env.SCAN_SAVING

    def assertPath(self, pattern):
        path = self.save_info.get_path()
        self.assertTrue(re.match(pattern, path), (path, pattern))

    def test_experiment(self):
        blissdata.newvisitor('HG94', verify=False)
        blissdata.newsample('sample1')
        blissdata.newdataset('area1')
        pattern = os.path.join(os.sep, 'data', 'visitor', 'hg94',
                               'id00', 'sample1', 'area1')
        self.assertPath(pattern)

        blissdata.newinhouse('HG94')
        pattern = os.path.join(os.sep, 'data', 'id00', 'inhouse',
                               '[0-9]{2}[a-z]{3}', 'hg94', 'id00',
                               'sample1', 'area1')
        self.assertPath(pattern)

        blissdata.newinhouse(None)
        pattern = os.path.join(os.sep, 'data', 'id00', 'inhouse', 'default',
                               'id00[0-9]{4}', 'id00', 'sample1',
                               'area1')
        self.assertPath(pattern)

        blissdata.newexperiment(None, root=self.dir.path)
        pattern = os.path.join(self.dir.path, anydir, 'id00',
                               'sample1', 'area1')
        self.assertPath(pattern)

    def test_sample(self):
        save_info = self.save_info
        with self.assertRaises(RuntimeError):
            blissdata.newsample('abc')
        blissdata.newexperiment(None, root=self.dir.path)
        with self.assertRaises(AttributeError):
            save_info.sample
        blissdata.newsample('abc')
        self.assertEqual(save_info.sample, 'abc')

    def test_dataset(self):
        save_info = self.save_info
        with self.assertRaises(RuntimeError):
            blissdata.newdataset('abc')
        blissdata.newexperiment(None, root=self.dir.path)
        with self.assertRaises(RuntimeError):
            blissdata.newdataset('abc')
        blissdata.newsample('abc')
        with self.assertRaises(AttributeError):
            save_info.dataset

        blissdata.newdataset()
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, '0001')
        blissdata.newdataset()
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, '0001')
        blissdata.newdataset('def')
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, 'def')
        blissdata.newdataset('def')
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, 'def')

        kwargs = {'sampleprefix': True, 'nonexisting': True}
        blissdata.newdataset(**kwargs)
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, 'abc_0001')
        blissdata.newdataset(10, **kwargs)
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, 'abc_0010')
        blissdata.newdataset(2, **kwargs)
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, 'abc_0011')
        blissdata.newdataset('def', **kwargs)
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, 'abc_def')
        blissdata.newdataset('def', **kwargs)
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, 'abc_def_0001')
        blissdata.newdataset('def_0010', **kwargs)
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, 'abc_def_0010')
        blissdata.newdataset('def', **kwargs)
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, 'abc_def_0011')


def test_suite():
    """Test suite including all test suites"""
    testSuite = unittest.TestSuite()
    testSuite.addTest(test_blissdata("test_experiment"))
    testSuite.addTest(test_blissdata("test_sample"))
    testSuite.addTest(test_blissdata("test_dataset"))
    return testSuite


if __name__ == '__main__':
    import sys

    mysuite = test_suite()
    runner = unittest.TextTestRunner()
    if not runner.run(mysuite).wasSuccessful():
        sys.exit(1)
