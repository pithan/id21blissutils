"""
Utilities for generating Bliss YAML configurations
"""
import os
from bliss.config import static


def node_filepath(filename, parent=None, subdir=None):
    """
    :param str filename:
    :param bliss.config.static.Node parent:
    :param str subdir:
    """
    if parent is not None:
        rootdir = os.path.dirname(parent.filename)
    else:
        rootdir = ''
    if not subdir:
        subdir = ''
    return os.path.join(rootdir, subdir, filename)


def _create_node(name, parent=None, subdir=None, mapping=None):
    """
    Create node in memory
    :param str name:
    :param bliss.config.static.Node parent:
    :param str subdir:
    :param dict mapping: node mapping
    :returns bliss.config.static.Node:
    """
    config = static.get_config()
    filename = node_filepath(name + '.yml', parent=parent, subdir=subdir)
    child = static.Node(config=config,
                        parent=parent,
                        filename=filename)
    if mapping:
        child.update(mapping)
    return child


def create_dirnode(name, parent=None, mapping=None):
    """
    Create node in memory

    :param str name:
    :param bliss.config.static.Node parent:
    :param dict mapping: node mapping
    :returns bliss.config.static.Node:
    """
    return _create_node('__init__', parent=parent,
                        subdir=name, mapping=mapping)


def create_filenode(name, parent=None, mapping=None):
    """
    Create node in memory

    :param str name:
    :param bliss.config.static.Node parent:
    :param dict mapping: node mapping
    :returns bliss.config.static.Node:
    """
    return _create_node(name, parent=parent, mapping=mapping)


def save_script(file_name, content):
    """
    Save setup or user script
    """
    content = '\n'.join(content)
    static.get_config().set_config_db_file(file_name, content)


def session_setup_filename(session_name):
    """
    :param str session_name:
    :returns str:
    """
    return session_name + '_setup.py'


def session_script_filename(session_name):
    """
    :param str session_name:
    :returns str:
    """
    return os.path.join('scripts', session_name + '.py')


def save_session_setup_script(session_name, parent):
    """
    Save script to be executed on session setup

    :param str session_name:
    :param bliss.config.static.Node parent:
    """
    content = ('from bliss.common.standard import *',
               'from bliss.setup_globals import *',
               '',
               'load_script(\'{}.py\')'.format(session_name),
               '', '')
    name = session_setup_filename(session_name)
    filename = node_filepath(name, parent=parent)
    save_script(filename, content)


def save_session_user_script(session_name, parent, content):
    """
    Save user script to be executed on session setup

    :param str session_name:
    :param bliss.config.static.Node parent:
    :param list content:
    """
    name = session_script_filename(session_name)
    filename = node_filepath(name, parent=parent)
    save_script(filename, content)


def session_mapping(session_name, session_objects=None, session_aliases=None):
    """
    :param str session_name:
    :param list session_objects:
    :param list session_aliases:
    """
    setup_filename = os.path.join('.', session_setup_filename(session_name))
    mapping = {"class": "Session",
               "name": session_name,
               "setup-file": setup_filename}
    if session_objects is not None:
        mapping['config-objects'] = session_objects
    if session_aliases is not None:
        mapping['aliases'] = session_aliases
    return mapping


def add_object(name, alias=None, session_aliases=None, **groups):
    """
    Add object to lists (session objects, measurment groups, ...)
    and session_aliases

    :param str name:
    :param list session_aliases: session
    :param **groups: str:list
    """
    for lst in groups.values():
        lst.append(name)
    if alias is None:
        alias = name
    if session_aliases is not None and name != alias:
        session_aliases.append({'original_name': name,
                                'alias_name': name,
                                'export_to_globals': False,
                                'remove_original': True})


def find_in_config(sclass, plugin=None):
    """
    Return a list of available sessions found in config

    :param str sclass:
    :param str plugin:
    """
    result = list()
    config = static.get_config()
    for name in config.names_list:
        c = config.get_config(name)
        if c.get("class") != sclass:
            continue
        if plugin:
            if c.get_inherited("plugin") != plugin:
                continue
        result.append(name)
    return result


def create_nonexisting(genfunc, prefix, **create_kwargs):
    """
    Creates a new test session in the YAML database (not in-memory yet)

    :param callable: genfunc
    :param str prefix:
    :returns str: session name
    """
    session_number = 1
    session_fmt = prefix + '_{}'
    sessions = find_in_config('Session', plugin='session')
    while session_fmt.format(session_number) in sessions:
        session_number += 1
    session_name = session_fmt.format(session_number)
    genfunc(session_name, **create_kwargs)
    return session_name
