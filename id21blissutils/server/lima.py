"""
Wrapper to launch a Bliss Beacon server
"""
import logging
import os
import gevent
from bliss.common.tango import DeviceProxy, DevFailed
from .server import Server


logger = logging.getLogger(__name__)


# https://github.com/esrf-bliss/Lima-tango-python/blob/df09b4e4/LimaCCDs.py#L82
level_to_cli = {logging.CRITICAL: 0,
                logging.ERROR: 1,
                logging.WARNING: 2,
                logging.INFO: 3,
                logging.DEBUG: 4}


class Lima(Server):
    """
    Run Bliss Lima server as a process
    """

    def __init__(self, device_name, tango_uri=None, **options):
        self._device_name = device_name
        self._tango_uri = tango_uri
        super(Lima, self).__init__('LimaCCDs', device_name, **options)

    @property
    def tango_uri(self):
        TANGO_HOST = os.environ.get('TANGO_HOST')
        tango_uri = self._tango_uri
        if not tango_uri:
            tango_uri = self._device_name
        if TANGO_HOST and not tango_uri.startswith('tango://'):
            while tango_uri.startswith('/'):
                tango_uri = tango_uri[1:]
            return 'tango://' + '/'.join([TANGO_HOST, tango_uri])
        else:
            return tango_uri

    def _kwargs_validate(self):
        if 'v' not in self._kwargs:
            level = logger.getEffectiveLevel()
            self._kwargs['v'] = level_to_cli.get(level, 'WARN')

    @property
    def proxy(self):
        return DeviceProxy(self.tango_uri)

    @property
    def isrunning(self):
        try:
            dev_proxy = self.proxy
            dev_proxy.ping()
            dev_proxy.state()
        except DevFailed:
            return False
        else:
            return True

    def _wait(self, start=True, timeout=None):
        if timeout is None:
            if not start and self._p is None:
                logger.debug('Lima server was started by someone else')
                return start == self.isrunning
        with gevent.Timeout(timeout):
            tango_uri = self.tango_uri
            logger.debug('Tango URI = {}'.format(tango_uri))
            while True:
                if start == self.isrunning:
                    return True
                gevent.sleep(1)
        return False

    @property
    def _env_generator(self):
        return
        yield
