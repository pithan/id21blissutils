"""
Wrapper base to launch a server sync/async
"""

import logging
import os
from gevent import subprocess, spawn
from abc import ABC, abstractmethod, abstractproperty

logger = logging.getLogger(__name__)


class CustomLogger(logging.LoggerAdapter):

    def process(self, msg, kwargs):
        return '[{}] {}'.format(self.extra['server'], msg), kwargs


class Server(ABC):
    """
    Run server sync or async
    """

    def __init__(self, *args, **kwargs):
        self._p = None
        self._async_func = None
        self.args = args
        self.kwargs = kwargs
        self.kwargmap = {}
        self.logger = CustomLogger(logger, {'server': str(self)})

    @property
    def args(self):
        return self._args

    @args.setter
    def args(self, value):
        if self.isrunning:
            logger.info('Changes will only be applied on restart')
        self._args = list(value)

    @property
    def kwargs(self):
        return self._kwargs

    @kwargs.setter
    def kwargs(self, value):
        if self.isrunning:
            logger.info('Changes will only be applied on restart')
        self._kwargs = dict(value)
        self._kwargs_validate()

    @abstractmethod
    def _kwargs_validate(self):
        pass

    @property
    def _async(self):
        return self._async_func is not None

    @property
    def isrunning(self):
        if self._p:
            if self._async:
                return not self._p.dead
            else:
                return self._p.poll() is None
        else:
            return False

    @property
    def _process_args(self):
        args = list(self._args)
        for k, v in self.kwargs.items():
            k = self.kwargmap.get(k, k)
            if k:
                if isinstance(v, bool):
                    if v:
                        args += ['--' + k]
                else:
                    args += ['--' + k, str(v)]
        return args

    @property
    def _greenlet_args(self):
        args = list(self._args)
        keys = map(lambda k: self.kwargmap.get(k, k), self.kwargs.keys())
        keys = [k for k in keys if k]
        kwargs = dict(zip(keys, self.kwargs.values()))
        return self._async_func, args, kwargs

    def start(self):
        if self.isrunning:
            return
        self.logger.info('Start ...')
        if self._async:
            func, args, kwargs = self._greenlet_args
            self._p = spawn(func, *args, **kwargs)
        else:
            self._p = subprocess.Popen(self._process_args)
        ret = self.wait_online(start=True, timeout=None)
        if ret:
            self.environment(load=True)
        return ret

    def __repr__(self):
        if self._async:
            type = 'AsyncThread'
        else:
            type = 'Process'
        return '{}({}): {}'.format(self.__class__.__name__, type, self.info)

    def stop(self, timeout=None):
        if not self.isrunning:
            return
        self.logger.info('Stop ...')
        if self._p is not None:
            if self._async:
                self._p.kill()
                self._p.join()
            else:
                self._p.terminate()
                self._p.wait()
        self._p = None
        ret = self.wait_online(start=False, timeout=timeout)
        if ret:
            self.environment(load=False)
        return ret

    @property
    def info(self):
        if self._async:
            func, args, kwargs = self._greenlet_args
            kwargs = ['{}={}'.format(k, v) for k, v in kwargs.items()]
            args = ', '.join(args)
            kwargs = ', '.join(kwargs)
            return '{}({}, {})'.format(func.__name__, args, kwargs)
        else:
            return ' '.join(self._process_args)

    @abstractmethod
    def _wait(self, start=True, timeout=None):
        pass

    def wait_online(self, start=True, timeout=None):
        if start:
            rstate = 'ONLINE'
        else:
            rstate = 'OFFLINE'
        ok = self._wait(start=start, timeout=timeout)
        if ok:
            self.logger.info('State is {}'.format(rstate))
        else:
            self.logger.warning('State is still {}'
                                .format(rstate))
        return ok

    @abstractproperty
    def _env_generator(self):
        pass

    def environment(self, load=True):
        """
        Set/unset the environment for clients to connect
        to a running server
        """
        lst = []
        for name, value in self._env_generator:
            if load:
                os.environ[name] = value
                lst.append('export {}={}'.format(name, value))
            else:
                del os.environ[name]
                lst.append('unset {}'.format(name))
        if lst:
            logger.info('Modified environment:\n ' + '; '.join(lst))
