import math
import itertools
import numpy


def divisorGenerator(n, includeone=True):
    for i in range(1, int(math.sqrt(n) + 1)):
        if n % i == 0:
            if includeone or i != 1:
                yield i
            if i*i != n:
                yield n // i


def asproduct(a, n, includeone=True):
    nums = divisorGenerator(a, includeone=includeone)
    for tpl in itertools.product(nums, repeat=n):
        if numpy.product(tpl) == a:
            yield tpl
