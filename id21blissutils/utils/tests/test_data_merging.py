import numpy
import unittest
import itertools
from .. import datamerging
from ...utils.math import asproduct


class test_data_merging(unittest.TestCase):

    def test_data_merging(self):
        shape = 4, 5
        dtype = float
        data = numpy.arange(numpy.product(shape), dtype=dtype)
        data = data.reshape(shape)
        axis = 0, 1, -1
        newaxis = True, False
        reshape = False, True
        order = 'C', 'F'
        advanced = False, True
        options = itertools.product(axis, newaxis, reshape, order, advanced)
        for axis, newaxis, reshape, order, advanced in options:
            kwargs = {'axis': axis,
                      'newaxis': newaxis,
                      'order': order,
                      'allow_advanced_indexing': advanced}
            with self.subTest(reshape=reshape, **kwargs):
                sources = [data]*3
                shapes = [shape]*3
                if not newaxis:
                    extrashape = list(shape)
                    extrashape[axis] *= 2
                    extrashape = tuple(extrashape)
                    extradata = numpy.arange(numpy.product(extrashape), dtype=dtype)
                    extradata = extradata.reshape(extrashape)
                    sources.append(extradata)
                    shapes.append(extrashape)
                if newaxis:
                    edata = numpy.stack(sources, axis=axis)
                else:
                    edata = numpy.concatenate(sources, axis=axis)
                if reshape:
                    dshape = edata.shape
                    it = asproduct(numpy.product(dshape), 3, includeone=False)
                    nshape = next(it)
                    while nshape == dshape:
                        nshape = next(it)
                    edata = edata.reshape(nshape, order=order)
                else:
                    nshape = None
                mshape, fillgen = datamerging.mergeGenerator(sources, shapes,
                                                             shape=nshape,
                                                             **kwargs)
                mdata = numpy.empty(shape=mshape, dtype=dtype)
                for ndarray, lstidxin, lstidxout in fillgen():
                        for idxin, idxout in zip(lstidxin, lstidxout):
                            for idx in idxin, idxout:
                                isadvanced = any(isinstance(x, numpy.ndarray) for x in idx)
                                self.assertTrue(isadvanced == (advanced and reshape))
                            mdata[idxout] = ndarray[idxin]
                numpy.testing.assert_array_equal(edata, mdata)


def test_suite():
    """Test suite including all test suites"""
    testSuite = unittest.TestSuite()
    testSuite.addTest(test_data_merging("test_data_merging"))
    return testSuite


if __name__ == '__main__':
    import sys

    mysuite = test_suite()
    runner = unittest.TextTestRunner()
    if not runner.run(mysuite).wasSuccessful():
        sys.exit(1)
