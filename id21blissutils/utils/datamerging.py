import numpy
import itertools


def _unravelIntListIndexing(idx1, idx2):
    """
    :param tuple(array) idx1: all array's have the same length
    :param tuple(array) idx2: all array's have the same length as `idx1`
    :return list(tuple), list(tuple):
    """
    # TODO: try creating slices
    idx1 = list(zip(*idx1))
    idx2 = list(zip(*idx2))
    return idx1, idx2


def _mergedShape(shapes, axis=0, newaxis=True):
    """
    Shape after merging

    :param list(tuple) shapes:
    :param int axis: merge along this axis
    :param bool newaxis: merge axis is new or existing
    :returns tuple or None:
    """
    if not shapes:
        return None
    shape = list(shapes[0])
    if newaxis:
        if axis < 0:
            axis += len(shape)+1
        shape.insert(axis, len(shapes))
    else:
        shape[axis] = 0
        for s in shapes:
            shape[axis] += s[axis]
    return tuple(shape)


def mergeShapeGenerator(sources, shapes, mshape, axis=0, newaxis=True):
    """
    Merge I/O index generator.

    :param list(any) sources:
    :param list(tuple) shapes: of sources
    :param tuple mshape: merged shape
    :param int axis: merge along this axis
    :param bool newaxis: merge axis is new or existing
    :returns generator: index generator
    """
    if len(sources) == 1:
        def fill_generator():
            yield sources[0], [tuple()], [tuple()]
    else:
        def fill_generator():
            idx = [slice(None)] * len(mshape)
            n = 0
            for i, (source, shapei) in enumerate(zip(sources, shapes)):
                if newaxis:
                    idx[axis] = i
                else:
                    idx[axis] = slice(n, n+shapei[axis])
                    n += shapei[axis]
                yield source, [tuple()], [tuple(idx)]
    return fill_generator


def mergeReshapeGenerator(sources, shapes, mshape, shape, order=None, axis=0,
                          newaxis=True, allow_advanced_indexing=True):
    """
    Reshaped merge I/O index generator.

    :param list(any) sources:
    :param list(tuple) shapes: of sources
    :param tuple mshape: merged shape
    :param tuple shape: reshaping mshape
    :param str order: for reshaping ('C': fast axis last, 'F': fast axis first)
    :param int axis: merge along this axis
    :param bool newaxis: merge axis is new or existing
    :param bool allow_advanced_indexing:
    :returns tuple, generator: merged shape and index generator
    """
    def fill_generator():
        ran = [list(range(n)) for n in mshape]
        n = 0
        for i, (uri, shapei) in enumerate(zip(sources, shapes)):
            # Flat indices for datain(shapei) and dataout(mshape)
            if newaxis:
                ran[axis] = [i]
            else:
                nnew = n + shapei[axis]
                ran[axis] = list(range(n, nnew))
                n = nnew
            if order == 'F':
                idxout = tuple(zip(*itertools.product(*ran[::-1])))[::-1]
            else:
                idxout = tuple(zip(*itertools.product(*ran)))
            idxout = numpy.ravel_multi_index(idxout, mshape, order=order)
            idxin = numpy.arange(numpy.product(shapei))
            # Multi indices for datain(shapei) and dataout(shape)
            idxin = numpy.unravel_index(idxin, shapei, order=order)
            idxout = numpy.unravel_index(idxout, shape, order=order)
            if allow_advanced_indexing:
                yield uri, [idxin], [idxout]
            else:
                idxout, idxin = _unravelIntListIndexing(idxout, idxin)
                yield uri, idxin, idxout
    return fill_generator


def mergeGenerator(sources, shapes, axis=0, newaxis=True,
                   shape=None, order=None, allow_advanced_indexing=True):
    """
    Equivalent to `numpy.stack` or `numpy.concatenate` combined
    with `numpy.reshape`. Return I/O index generator for merging.

    :param list(any) sources:
    :param list(tuple) shapes: of sources
    :param int axis: merge along this axis
    :param bool newaxis: merge axis is new or existing
    :param tuple shape: for reshaping
    :param str order: for reshaping
    :param bool allow_advanced_indexing:
    :returns tuple, generator: merged shape and index generator
    """
    mshape = _mergedShape(shapes, axis=axis, newaxis=newaxis)
    if not mshape:
        def fill_generator():
            return
            yield None
    elif shape is None or shape == mshape:
        fill_generator = mergeShapeGenerator(sources, shapes, mshape,
                                             axis=axis, newaxis=newaxis)
        shape = mshape
    else:
        fill_generator = mergeReshapeGenerator(sources, shapes, mshape, shape,
                                               order=order, axis=axis,
                                               newaxis=newaxis,
                                               allow_advanced_indexing=allow_advanced_indexing)
    return shape, fill_generator
