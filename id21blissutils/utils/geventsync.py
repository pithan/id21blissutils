"""
GEvent synchronization utilities
"""
from contextlib import contextmanager, ExitStack
import gevent.lock


class SharedLockPool(object):
    """
    Allows to acquire locks identified by name recursively.
    """

    def __init__(self):
        self.__locks = {}
        self.__locks_mutex = gevent.lock.Semaphore(value=1)

    @contextmanager
    def _locks(self):
        self.__locks_mutex.acquire()
        try:
            yield self.__locks
        finally:
            self.__locks_mutex.release()

    def pop(self, name):
        with self._locks() as locks:
            return locks.pop(name, None)

    @contextmanager
    def acquire(self, name):
        with self._locks() as locks:
            lock = locks.get(name, None)
            if lock is None:
                locks[name] = lock = gevent.lock.RLock()
        lock.acquire()
        try:
            yield
        finally:
            lock.release()

    @contextmanager
    def acquire_context_creation(self, name, contextmngr, *args, **kwargs):
        """
        Acquire lock only during context creation.

        This can be used for example to propect the opening of a file
        but not hold the lock while the file is open.
        """
        with ExitStack() as stack:
            with self.acquire(name):
                ret = stack.enter_context(contextmngr(*args, **kwargs))
            yield ret
