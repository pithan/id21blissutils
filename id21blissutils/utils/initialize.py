"""
Setup in user script
"""
import logging
from ..data.generator import initialize
from .config import scan_saving

__all__ = ['id21blissutils_setup']


logger = logging.getLogger(__name__)


def id21blissutils_setup():
    """
    Setup ID21 Bliss utilities (call in user script of the Bliss session)
    """
    logger.debug('Setup ID21 Bliss utilities')
    # Redis metadata generators
    scan_saving()
    initialize()
