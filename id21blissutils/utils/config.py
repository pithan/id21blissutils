"""
Bliss session configuration utilities
"""
import os
import re
import logging
from copy import deepcopy
from collections import defaultdict
from bliss.common.session import get_current_session
from bliss.config import static
from glob import glob
from ..data import naming


logger = logging.getLogger(__name__)


__all__ = ['techniques', 'datasets']


def static_config():
    """
    Get static config

    :returns bliss.config.static.Config:
    """
    return static.get_config()


def static_config_root():
    """
    Get root node of session's static config

    :returns bliss.config.static.Node:
    """
    return static_config().root


def init_scan_saving(scan_saving):
    """
    Initialize session's SCAN_SAVING object (add missing attributes)

    :param bliss.scanning.scan.ScanSaving scan_saving:
    """
    try:
        scan_saving.beamline
    except AttributeError:
        bl = ''
        for k in 'BEAMLINE', 'BEAMLINENAME':
            bl = os.environ.get(k, bl)
        bl = static_config_root().get('beamline', bl)
        if not bl:
            bl = 'id00'
        scan_saving.add('beamline', bl.lower())
    try:
        scan_saving.technique
    except AttributeError:
        scan_saving.add('technique', default_technique())
    try:
        scan_saving.inhouse_folder
    except AttributeError:
        scan_saving.add('inhouse_folder', default_inhouse_folder())
    try:
        scan_saving.mdataon
    except:
        scan_saving.add('mdataon', False)
    scan_saving.data_filename = '_bliss'
    scan_saving.writer = 'null'  # TODO: This is necessary for setting the device paths


def scan_saving():
    """
    Get session's SCAN_SAVING object

    :returns bliss.scanning.scan.ScanSaving:
    """
    o = get_current_session().env_dict['SCAN_SAVING']
    init_scan_saving(o)
    return o


def last_scans():
    """
    :returns list(str):
    """
    return get_current_session().env_dict.get('SCANS', [])


def scan_saving_pathinfo():
    """
    Get path information from the session's SCAN_SAVING object

    :returns dict:
    """
    # SCAN_SAVING.to_dict() may fail for unrelated reasons
    # so extract only the information we need
    params = {}
    _scan_saving = scan_saving()
    try:
        params['base_path'] = _scan_saving.base_path
    except AttributeError:
        return params
    try:
        params['template'] = template = _scan_saving.template
    except AttributeError:
        return params
    for attr in re.findall(r'\{(.*?)\}', template):
        try:
            params[attr] = getattr(_scan_saving, attr)
        except AttributeError:
            pass
    return params


def scan_saving_path():
    """
    Get path from the session's SCAN_SAVING object

    :returns str:
    :raises RuntimeError: missing information
    """
    params = scan_saving_pathinfo()
    try:
        return os.path.join(params['base_path'],
                            params['template'].format(**params))
    except KeyError as e:
        raise RuntimeError("Missing '{}' attribute in SCAN_SAVING"
                           .format(e))


def scan_saving_get(attr, default=None):
    """
    Get attribute from the session's scan saving object

    :returns str:
    """
    return getattr(scan_saving(), attr, default)


def scan_saving_set(attr, value):
    """
    Get attribute from the session's scan saving object

    :param str attr:
    :param value: must be picklable?
    """
    scan_saving().add(attr, value)


def find_static_name(name, parent=None):
    """
    :param bliss.config.static.Node node:
    :returns dict:
    """
    if parent is None:
        parent = static_config_root()
    if parent.children:
        for node in parent.children:
            if node.get('name', None) == name:
                return node.to_dict()
    nodes = []
    for node in parent.values():
        if isinstance(node, static.Node):
            nodes.append(node)
        elif isinstance(node, list):
            for nodei in node:
                if isinstance(nodei, static.Node):
                    nodes.append(nodei)
    for node in nodes:
        if node.get('name', None) == name:
            return node.to_dict()
        ret = find_static_name(name, parent=node)
        if ret:
            return ret
    return {}


# TODO: issues with YAML parsing in Bliss
#       exception on the first try but not 
#       for subsequent tries (result is mangled)
_STATIC_SCAN_INFO_RAISE = True


def static_scan_info():
    """
    Scan saving info from the session configuration

    :returns dict:
    """
    global _STATIC_SCAN_INFO_RAISE
    try:
        if _STATIC_SCAN_INFO_RAISE:
            raise RuntimeError
        return static_config().get('scan_info').to_dict()
    except (RuntimeError, KeyError):
        _STATIC_SCAN_INFO_RAISE = True
        #logger.info("Cannot load 'scan_info' from session configuration")
        return find_static_name('scan_info')


def default_inhouse_folder():
    """
    Inhouse folder name from the session configuration

    :returns dict:
    """
    return static_scan_info().get('inhouse', 'inhouse')


def static_technique_info():
    """
    Information on techniques from the session configuration

    :returns dict:
    """
    return static_scan_info().get('technique', {})


def default_technique():
    """
    Default technique from the session configuration

    :returns str:
    """
    return static_technique_info().get('default', 'undefined')


def current_technique():
    """
    Active technique from the session's scan saving object

    :returns str:
    """
    return scan_saving_get('technique')  # no default needed


def techniques():
    """
    List of available techniques from the session configuration

    :returns list:
    """
    return list(static_technique_info().get('techniques', {}).keys())


def technique_definition(technique):
    """
    Technique deifnition from the static session configuration

    :param str techique:
    :returns dict: {'name': technique,
                    'applications': dict(dict),
                    'plots': dict(list),
                    'plotselect': str}
    """
    applications = {}
    plots = {}
    ret = {'name': technique,
           'applications': applications,
           'plots': plots,
           'plotselect': ''}
    technique = static_technique_info().get('techniques', {}).get(technique, {})

    # Get the application definitions specified for this technique in YAML
    applicationdict = static_technique_info().get('applications', {})
    for name in technique.get('applications', []):
        definition = applicationdict.get(name, {})
        # for example {'xrf':{'I0': 'iodet',
        #                     'It': 'idet',
        #                     'mca': [...]}, ...}
        if definition:
            name = definition.pop('personal_name', name)
            applications[name] = definition

    # Get the plots specified for this technique in YAML
    plotdict = static_technique_info().get('plots', {})
    plotselect = ''
    for name in technique.get('plots', []):
        plotdefinition = plotdict.get(name, {})
        # for examples:
        #   {'personal_name': 'counters', 'items': ['iodet', 'xmap1:deadtime_det2', ...]}
        #   {'personal_name': 'counters', 'ndim': 2, 'grid': True}
        items = plotdefinition.get('items', [])
        ndim = plotdefinition.get('ndim', -1)
        grid = plotdefinition.get('grid', False)
        if not items and ndim < 0:
            continue
        name = plotdefinition.get('personal_name', name)
        if name in applications:
            name = name + '_plot'
        if not plotselect:
            plotselect = name
        plots[name] = {'items': items, 'grid': grid, 'ndim': ndim}
    ret['plotselect'] = plotselect
    return ret


def current_technique_definition():
    """
    Current technique deifnition from the static session configuration

    :returns dict(dict): technique:definition (str:dict)
    """
    return technique_definition(current_technique())


def filenames(**replace):
    """
    HDF5 file names to be save by the external writer.
    The first is to write and the other are masters to link.

    :returns list(str):
    """
    params = scan_saving_pathinfo()
    for k, v in replace.items():
        params[k] = v
    name_templates = naming.static_filename_templates()
    try:
        base_path = os.path.join(params['base_path'],
                                 params['template'].format(**params))
    except KeyError:
        paths = [''] * len(name_templates)
    else:
        relpath = '.'
        paths = []
        for name_template in name_templates:
            if name_template:
                try:
                    filename = name_template.format(**params)
                except KeyError:
                    paths.append('')
                else:
                    filename = os.path.join(base_path, relpath, filename)
                    paths.append(os.path.normpath(filename))
            else:
                paths.append('')
            relpath = os.path.join('..', relpath)
        if not paths[0]:
            paths = [''] * len(name_templates)
            paths[0] = os.path.join(base_path, 'external.h5')
    return paths


def samples():
    """
    Current proposal samples

    :returns list(str):
    """
    lst = []
    searchpattern = filenames(sample='*')[1]
    if searchpattern:
        for filename in sorted(glob(searchpattern)):
            lst.append(os.path.basename(os.path.dirname(filename)))
    return list(sorted(set(lst)))


def datasets(sample=None):
    """
    Current proposal samples

    :returns list(str):
    """
    lst = []
    if sample:
        samplepattern = sample
    else:
        samplepattern = '*'
    searchpattern = filenames(sample=samplepattern, dataset='*')[0]
    if searchpattern:
        for filename in sorted(glob(searchpattern)):
            dataset = os.path.basename(os.path.dirname(filename))
            if sample:
                if dataset.startswith(sample + '_'):
                    dataset = dataset[len(sample)+1:]
            lst.append(dataset)
    return list(sorted(set(lst)))


def dataset_filenames(*names, current=True, sample=None, experiment=None):
    """
    :param names: dataset names
    :param bool current: use current when no names provided
    :returns list(str)):
    """
    if names:
        if not sample and not current:
            sample = '*'
    else:
        if current:
            names = [scan_saving_get('dataset', '*')]
        else:
            names = ['*']
    lst = []
    kwargs = {}
    if sample:
        kwargs['sample'] = sample
    if experiment:
        kwargs['experiment'] = experiment
    for name in names:
        kwargs['dataset'] = name
        searchpattern = filenames(**kwargs)[0]
        if searchpattern:
            lst += sorted(glob(searchpattern))
    lst = [filename for filename in lst if os.path.basename(filename) != '_bliss.h5']
    return lst


def sample_filenames(*names, current=True, experiment=None):
    """
    :param names: sample names
    :param bool current: use current when no names provided
    :returns list(str)):
    """
    if not names:
        if current:
            names = [scan_saving_get('sample', '*')]
        else:
            names = ['*']
    lst = []
    kwargs = {}
    if experiment:
        kwargs['experiment'] = experiment
    for name in names:
        kwargs['sample'] = name
        searchpattern = filenames(sample=name)[1]
        if searchpattern:
            lst += sorted(glob(searchpattern))
    return lst


def experiment_filenames(*names, current=True):
    """
    :param names: proposal names
    :param bool current: use current when no names provided
    :returns list(str)):
    """
    if not names:
        if current:
            names = [scan_saving_get('experiment', '*')]
        else:
            names = ['*']
    lst = []
    for name in names:
        searchpattern = filenames(experiment=name)[2]
        if searchpattern:
            lst += sorted(glob(searchpattern))
    return lst
