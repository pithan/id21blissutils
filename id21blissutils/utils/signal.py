"""
Capture signal in Greenlets
"""

import gevent
from gevent import signal


def appendsignal(sig, func):
    oldhandler = signal.getsignal(sig)
    
    def newhandler(*args):
        try:
            print(func)
            func()
        finally:
            gevent.signal(sig, oldhandler)
    gevent.signal(sig, newhandler)


def killonexit(greenlet):
    for sig in signal.SIGQUIT, signal.SIGINT:
        appendsignal(sig, greenlet.kill)
