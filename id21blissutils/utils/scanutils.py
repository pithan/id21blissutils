"""
Bliss scan utilities
"""
from . import config
from ..data import naming


def filenames(info, default=None):
    """
    :param bliss.scanning.scan.Scan or dict scan_info:
    :returns list(str):
    """
    if not isinstance(info, dict):
        info = info.scan_info
    try:
        lst = info['external']['filenames']
    except KeyError:
        lst = [''] * len(naming.static_filename_templates())
    if default and not lst[0]:
        lst[0] = default
    return lst


def scan_name(info, subscan=1):
    """
    :param bliss.scanning.scan.Scan or dict scan_info:
    :returns str:
    """
    if not isinstance(info, dict):
        info = info.scan_info
    return '{}.{}'.format(info['scan_nb'], subscan)
    #return '{}. {}'.format(info['scan_nb'],
    #                       info['title'])


def scan_uri(info, subscan=1):
    """
    :param bliss.scanning.scan.Scan or dict scan_info:
    :returns str:
    """
    if not isinstance(info, dict):
        info = info.scan_info
    filename = filenames(info)[0]
    if filename:
        return filename + '::/' + scan_name(info, subscan=subscan)
    else:
        return ''


def dataset_uri(info):
    """
    :param bliss.scanning.scan.Scan or dict scan_info:
    :returns str:
    """
    return filenames(info)[0]


def last_uri():
    """
    Last scan with saving enabled or current dataset

    :returns str: scan or dataset uri
    """
    for scan in reversed(config.last_scans()):
        if scan.scan_info['save']:
            return scan_uri(scan)
    return config.filenames()[0]


def sample_uri(info):
    """
    :param bliss.scanning.scan.Scan or dict scan_info:
    :returns str:
    """
    return filenames(info)[1]


def experiment_uri(info):
    """
    :param bliss.scanning.scan.Scan or dict scan_info:
    :returns str:
    """
    return filenames(info)[2]
