import unittest
from testfixtures import TempDirectory
import os
import numpy
from contextlib import contextmanager
import h5py.h5t
from .. import nexus


class test_nexus(unittest.TestCase):

    def setUp(self):
        self.dir = TempDirectory()

    def tearDown(self):
        self.dir.cleanup()

    @contextmanager
    def h5open(self, name):
        filename = os.path.join(self.dir.path, name+'.h5')
        with nexus.nxRoot(filename, mode='a') as h5group:
            yield h5group

    def validateNxRoot(self, h5group):
        attrs = ['NX_class', 'creator', 'HDF5_Version', 'file_name',
                 'file_time', 'file_update_time', 'h5py_version']
        self.assertEqual(set(h5group.attrs.keys()), set(attrs))
        self.assertEqual(h5group.attrs['NX_class'], 'NXroot')
        self.assertEqual(h5group.name, '/')

    def validateNxEntry(self, h5group):
        attrs = ['NX_class']
        self.assertEqual(set(h5group.attrs.keys()), set(attrs))
        files = ['start_time', 'end_time']
        self.assertEqual(set(h5group.keys()), set(files))
        self.assertEqual(h5group.attrs['NX_class'], 'NXentry')
        self.assertEqual(h5group.parent.name, '/')

    def validateNxProcess(self, h5group):
        attrs = ['NX_class']
        self.assertEqual(set(h5group.attrs.keys()), set(attrs))
        files = ['program', 'version', 'configuration', 'date', 'results']
        self.assertEqual(set(h5group.keys()), set(files))
        self.assertEqual(h5group.attrs['NX_class'], 'NXprocess')
        self.assertEqual(h5group.parent.attrs['NX_class'], 'NXentry')
        self.validateNxNote(h5group['configuration'])
        self.validateNxCollection(h5group['results'])

    def validateNxNote(self, h5group):
        attrs = ['NX_class']
        self.assertEqual(set(h5group.attrs.keys()), set(attrs))
        files = ['date', 'data', 'type']
        self.assertEqual(set(h5group.keys()), set(files))
        self.assertEqual(h5group.attrs['NX_class'], 'NXnote')

    def validateNxCollection(self, h5group):
        attrs = ['NX_class']
        self.assertEqual(set(h5group.attrs.keys()), set(attrs))
        self.assertEqual(h5group.attrs['NX_class'], 'NXcollection')

    def validateNxData(self, h5group, axes, signals):
        attrs = ['NX_class', 'axes', 'signal', 'auxiliary_signals']
        self.assertEqual(set(h5group.attrs.keys()), set(attrs))
        files = list(next(iter(zip(*axes)))) + list(next(iter(zip(*signals))))
        self.assertEqual(set(h5group.keys()), set(files))
        self.assertEqual(h5group.attrs['NX_class'], 'NXdata')

    def test_nexus_Root(self):
        with self.h5open('test_nexus_Root') as h5group:
            self.validateNxRoot(h5group)

    def test_nexus_Entry(self):
        with self.h5open('test_nexus_Entry') as h5group:
            entry = nexus.nxEntry(h5group, 'entry0001')
            nexus.updated(entry, final=True)
            self.assertRaises(RuntimeError, nexus.nxEntry,
                              entry, 'entry0002')
            self.validateNxEntry(entry)

    def test_nexus_Process(self):
        with self.h5open('test_nexus_Process') as h5group:
            entry = nexus.nxEntry(h5group, 'entry0001')
            configdict = {'a': 1, 'b': 2}
            for i, type in enumerate(['json', 'ini', None]):
                process = nexus.nxProcess(entry, 'process{:04d}'.format(i),
                                          configdict=configdict, type=type)
                self.assertRaises(RuntimeError, nexus.nxProcess,
                                  h5group, 'process0002',
                                  configdict=configdict)
                self.validateNxProcess(process)

    def test_nexus_Data(self):
        with self.h5open('test_nexus_Data') as h5group:
            entry = nexus.nxEntry(h5group, 'entry0001')
            process = nexus.nxProcess(entry, 'process0001')
            data = nexus.nxData(process['results'], 'data')
            s = (4, 3, 2)
            datadict = {'Fe K': numpy.arange(numpy.product(s), dtype=float).reshape(s),
                        'Ca K': numpy.arange(numpy.product(s)).reshape(s)+1,
                        'S K': numpy.zeros(s)}
            axes = [('y', numpy.arange(s[0]), {'units': 'um'}),
                    ('x', numpy.arange(s[1]), {}),
                    ('z', {'shape': (s[2],), 'dtype': int}, None)]
            signals = [('Fe K', datadict['Fe K'], {'interpretation': 'image'}),
                       ('Ca K', {'data': datadict['Ca K']}, {}),
                       ('S K', {'shape': s, 'dtype': int}, None)]
            nexus.nxDataAddAxes(data, axes)
            nexus.nxDataAddSignals(data, signals)

            self.validateNxData(data, axes, signals)
            signals = nexus.nxDataGetSignals(data)
            self.assertEqual(signals, ['Fe K', 'Ca K', 'S K'])

            nexus.markDefault(data['Ca K'])
            data = entry[nexus.DEFAULT_PLOT_NAME]
            signals = nexus.nxDataGetSignals(data)
            self.assertEqual(signals, ['Ca K', 'Fe K', 'S K'])
            self.assertEqual(data['y'].attrs['units'], 'um')
            self.assertEqual(data['Fe K'].attrs['interpretation'], 'image')
            for name in signals:
                self.assertEqual(data[name].shape, s)
            for n, name in zip(s, list(next(iter(zip(*axes))))):
                self.assertEqual(data[name].shape, (n,))

            # Test dataset concatenation
            def vdatanamegen():
                c = 0
                while True:
                    yield 'vdata{}'.format(c)
                    c += 1
            vdataname = vdatanamegen()
            for virtual in False, True:
                value = {'axis': 0,
                         'newaxis': True,
                         'virtual': virtual,
                         'data': [nexus.getUri(data[name]) for name in datadict]}
                vdata = nexus.nxCreateDataSet(process, next(vdataname), value, None)
                for i, name in enumerate(datadict):
                    numpy.testing.assert_array_equal(datadict[name], vdata[i])
                value['axis'] = 1
                vdata1 = nexus.nxCreateDataSet(process, next(vdataname), value, None)
                for i, name in enumerate(datadict):
                    numpy.testing.assert_array_equal(datadict[name], vdata1[:, i])
                value['axis'] = 0
                value['newaxis'] = False
                vdata = nexus.nxCreateDataSet(process, next(vdataname), value, None)
                for i, name in enumerate(datadict):
                    numpy.testing.assert_array_equal(datadict[name],
                                                     vdata[i*s[0]:(i+1)*s[0]])
                value['axis'] = 1
                vdata = nexus.nxCreateDataSet(process, next(vdataname), value, None)
                for i, name in enumerate(datadict):
                    numpy.testing.assert_array_equal(datadict[name],
                                                     vdata[:, i*s[1]:(i+1)*s[1]])
                value['data'].append(nexus.getUri(data['y']))
                with self.assertRaises(RuntimeError):
                    nexus.nxCreateDataSet(process, next(vdataname), value, None)

    def test_nexus_StringAttribute(self):
        self._checkStringTypes(attribute=True, raiseExtended=True)

    def test_nexus_StringDataset(self):
        self._checkStringTypes(attribute=False, raiseExtended=True)

    def test_nexus_ExtStringAttribute(self):
        self._checkStringTypes(attribute=True, raiseExtended=False)

    def test_nexus_ExtStringDataset(self):
        self._checkStringTypes(attribute=False, raiseExtended=False)

    def _checkStringTypes(self, attribute=True, raiseExtended=True):
        # Test following string literals
        sAsciiBytes = b'abc'
        sAsciiUnicode = u'abc'
        sLatinBytes = b'\xe423'
        sLatinUnicode = u'\xe423'  # not used
        sUTF8Unicode = u'\u0101bc'
        sUTF8Bytes = b'\xc4\x81bc'
        sUTF8AsciiUnicode = u'abc'
        sUTF8AsciiBytes = b'abc'
        # Expected conversion after HDF5 write/read
        strmap = {}
        strmap['ascii(scalar)'] = sAsciiBytes, sAsciiUnicode
        strmap['ext(scalar)'] = sLatinBytes, sLatinBytes
        strmap['unicode(scalar)'] = sUTF8Unicode, sUTF8Unicode
        strmap['unicode2(scalar)'] = sUTF8AsciiUnicode, sUTF8AsciiUnicode
        strmap['ascii(list)'] = [sAsciiBytes, sAsciiBytes],\
                                [sAsciiUnicode, sAsciiUnicode]
        strmap['ext(list)'] = [sLatinBytes, sLatinBytes],\
                              [sLatinBytes, sLatinBytes]
        strmap['unicode(list)'] = [sUTF8Unicode, sUTF8Unicode],\
                                  [sUTF8Unicode, sUTF8Unicode]
        strmap['unicode2(list)'] = [sUTF8AsciiUnicode, sUTF8AsciiUnicode],\
                                   [sUTF8AsciiUnicode, sUTF8AsciiUnicode]
        strmap['mixed(list)'] = [sUTF8Unicode, sUTF8AsciiUnicode, sAsciiBytes, sLatinBytes],\
                                [sUTF8Bytes, sUTF8AsciiBytes, sAsciiBytes, sLatinBytes]
        strmap['ascii(0d-array)'] = numpy.array(sAsciiBytes), sAsciiUnicode
        strmap['ext(0d-array)'] = numpy.array(sLatinBytes), sLatinBytes
        strmap['unicode(0d-array)'] = numpy.array(sUTF8Unicode), sUTF8Unicode
        strmap['unicode2(0d-array)'] = numpy.array(sUTF8AsciiUnicode), sUTF8AsciiUnicode
        strmap['ascii(1d-array)'] = (numpy.array([sAsciiBytes, sAsciiBytes]),
                                     [sAsciiUnicode, sAsciiUnicode])
        strmap['ext(1d-array)'] = (numpy.array([sLatinBytes, sLatinBytes]),
                                   [sLatinBytes, sLatinBytes])
        strmap['unicode(1d-array)'] = (numpy.array([sUTF8Unicode, sUTF8Unicode]),
                                       [sUTF8Unicode, sUTF8Unicode])
        strmap['unicode2(1d-array)'] = (numpy.array([sUTF8AsciiUnicode, sUTF8AsciiUnicode]),
                                        [sUTF8AsciiUnicode, sUTF8AsciiUnicode])
        strmap['mixed(1d-array)'] = (numpy.array([sUTF8Unicode, sUTF8AsciiUnicode, sAsciiBytes]),
                                     [sUTF8Unicode, sUTF8AsciiUnicode, sAsciiUnicode])
        strmap['mixed2(1d-array)'] = (numpy.array([sUTF8AsciiUnicode, sAsciiBytes]),
                                      [sUTF8AsciiUnicode, sAsciiUnicode])

        with self.h5open('test_nexus_String{:d}'.format(attribute)) as h5group:
            h5group = h5group.create_group('test')
            if attribute:
                out = h5group.attrs
            else:
                out = h5group
            for name, (value, expectedValue) in strmap.items():
                decodingError = 'ext' in name or name == 'mixed(list)'
                if raiseExtended and decodingError:
                    with self.assertRaises(UnicodeDecodeError):
                        ovalue = nexus.asNxChar(value,
                                                raiseExtended=raiseExtended)
                    continue
                else:
                    ovalue = nexus.asNxChar(value,
                                            raiseExtended=raiseExtended)
                # Write/read
                out[name] = ovalue
                if attribute:
                    value = out[name]
                else:
                    value = out[name][()]
                # Expected type and value?
                if 'list' in name or '1d-array' in name:
                    self.assertTrue(isinstance(value, numpy.ndarray))
                    value = value.tolist()
                    self.assertEqual(list(map(type, value)),
                                     list(map(type, expectedValue)), msg=name)
                    firstValue = value[0]
                else:
                    firstValue = value
                msg = '{} {} instead of {}'.format(name, type(value), type(expectedValue))
                self.assertEqual(type(value), type(expectedValue), msg=msg)
                self.assertEqual(value, expectedValue, msg=name)
                # Expected character set?
                if not attribute:
                    charSet = out[name].id.get_type().get_cset()
                    if isinstance(firstValue, bytes):
                        # This is the tricky part, CSET_ASCII is supposed to be only 0-127
                        # while we actually allow
                        expectedCharSet = h5py.h5t.CSET_ASCII
                    else:
                        expectedCharSet = h5py.h5t.CSET_UTF8
                    msg = '{} type {} instead of {}'.format(name, charSet, expectedCharSet)
                    self.assertEqual(charSet, expectedCharSet, msg=msg)

    def test_uri(self):
        path = self.dir.path

        uri = 'test1.h5::/a::/b'
        a, b = nexus.splitUri(uri)
        self.assertEqual(a, 'test1.h5')
        self.assertEqual(b, '/a::/b')

        uri = nexus.normUri('./test1.h5::/a/../b')
        self.assertEqual(uri, 'test1.h5::/b')

        uri = 'test1.h5::/a/b'
        uriref = 'test1.h5::/a'
        a, b = nexus.relUri(uri, uriref)
        self.assertEqual(a, '.')
        self.assertEqual(b, 'b')

        uri = os.path.join(path, 'test1.h5::/a/b')
        uriref = os.path.join(path, 'test1.h5::/a')
        a, b = nexus.relUri(uri, uriref)
        self.assertEqual(a, '.')
        self.assertEqual(b, 'b')

        uri = 'test1.h5::/a/b'
        uriref = 'test2.h5::/a'
        a, b = nexus.relUri(uri, uriref)
        self.assertEqual(a, 'test1.h5')
        self.assertEqual(b, '/a/b')

        path = self.dir.path
        uri = os.path.join(path, 'test1.h5::/a/b')
        uriref = os.path.join(path, 'test2.h5::/a')
        a, b = nexus.relUri(uri, uriref)
        self.assertEqual(a, './test1.h5')
        self.assertEqual(b, '/a/b')

        path = self.dir.path
        uri = os.path.join(path, '..', 'test1.h5::/a/b')
        uriref = os.path.join(path, 'test2.h5::/a')
        a, b = nexus.relUri(uri, uriref)
        self.assertEqual(a, '../test1.h5')
        self.assertEqual(b, '/a/b')

    def test_links(self):
        def namegen():
            i = 1
            while True:
                yield 'link' + str(i)
                i += 1
        linkname = namegen()
        with self.h5open(os.path.join('a', 'b', 'test1')) as f1:
                f1.create_group('a/b/c')
                g = f1['/a/b']
                # internal link up
                name = next(linkname)
                nexus.createLink(g, name, f1['a'])
                link = g.get(name, getlink=True)
                self.assertEqual(link.path, '/a')
                self.assertTrue(isinstance(link, h5py.SoftLink))
                # internal link same level
                name = next(linkname)
                nexus.createLink(g, name, f1['a/b'])
                link = g.get(name, getlink=True)
                self.assertEqual(link.path, '.')
                self.assertTrue(isinstance(link, h5py.SoftLink))
                # internal link down
                name = next(linkname)
                nexus.createLink(g, name, f1['a/b/c'])
                link = g.get(name, getlink=True)
                self.assertEqual(link.path, 'c')
                self.assertTrue(isinstance(link, h5py.SoftLink))
                # external link down
                with self.h5open(os.path.join('a', 'test2')) as f2:
                    name = next(linkname)
                    nexus.createLink(f2, name, f1['a'])
                    link = f2.get(name, getlink=True)
                    self.assertEqual(link.path, '/a')
                    self.assertEqual(link.filename, 'b/test1.h5')
                    self.assertTrue(isinstance(link, h5py.ExternalLink))
                # internal link same level
                with self.h5open(os.path.join('a', 'b', 'test2')) as f2:
                    name = next(linkname)
                    nexus.createLink(f2, name, f1['a'])
                    link = f2.get(name, getlink=True)
                    self.assertEqual(link.path, '/a')
                    self.assertEqual(link.filename, './test1.h5')
                    self.assertTrue(isinstance(link, h5py.ExternalLink))
                # external link up
                with self.h5open(os.path.join('a', 'b', 'c', 'test2')) as f2:
                    name = next(linkname)
                    nexus.createLink(f2, name, f1['a'])
                    link = f2.get(name, getlink=True)
                    self.assertEqual(link.path, '/a')
                    self.assertEqual(link.filename, '../test1.h5')
                    self.assertTrue(isinstance(link, h5py.ExternalLink))

    @unittest.skipIf(not nexus.HASVIRTUAL, 'h5py does not support virtual datasets')
    def test_reshape_datasets(self):
        shape = 12, 5
        vshape = 3, 4, 5
        order = 'C'
        kwargs = {'axis': 0, 'virtual': True, 'newaxis': False,
                  'shape': vshape, 'order': order, 'fillvalue': 0}
        datamem = numpy.arange(numpy.product(shape)).reshape(shape, order=order)
        filenames = os.path.join('basedir1', 'test1'),\
                    os.path.join('basedir1', 'test2'),\
                    os.path.join('basedir1', 'subdir', 'test3')
        with self.h5open(filenames[0]) as root1:
            with self.h5open(filenames[1]) as root2:
                with self.h5open(filenames[2]) as root3:
                    for root in root1, root2, root3:
                        g = root.create_group('a')
                        g.create_group('b')
                        g['data'] = datamem
                        # Internal links
                        kwargs['data'] = [nexus.getUri(root['/a/data'])]
                        nexus.nxCreateDataSet(root, 'vdata', kwargs, None)
                        nexus.nxCreateDataSet(root['/a'], 'vdata', kwargs, None)
                        nexus.nxCreateDataSet(root['/a/b'], 'vdata', kwargs, None)
                    # root1 -> root2, root3
                    kwargs['data'] = [nexus.getUri(root1['/a/data'])]
                    for root in root2, root3:
                        nexus.nxCreateDataSet(root, 'vdatae', kwargs, None)
                        nexus.nxCreateDataSet(root['/a'], 'vdatae', kwargs, None)
                        nexus.nxCreateDataSet(root['/a/b'], 'vdatae', kwargs, None)
                    # root2 -> root1
                    kwargs['data'] = [nexus.getUri(root2['/a/data'])]
                    nexus.nxCreateDataSet(root1, 'vdatae', kwargs, None)
                    nexus.nxCreateDataSet(root1['/a'], 'vdatae', kwargs, None)
                    nexus.nxCreateDataSet(root1['/a/b'], 'vdatae', kwargs, None)

        def flatten(arr):
            return arr.flatten(order=order)
        datamem = flatten(datamem)
        paths = ('/vdata', '/vdatae',
                 '/a/vdata', '/a/vdatae',
                 '/a/b/vdata', '/a/b/vdatae')
        for filename in filenames:
            with self.h5open(filename) as root:
                data = root['/a/data']
                self.assertEqual(shape, data.shape)
                numpy.testing.assert_array_equal(datamem, flatten(data[()]))
                for path in paths:
                    vdata = root[path]
                    self.assertEqual(vshape, vdata.shape)
                    numpy.testing.assert_array_equal(datamem,
                                                     flatten(vdata[()]),
                                                     err_msg=nexus.getUri(vdata))

        os.rename(os.path.join(self.dir.path, 'basedir1'),
                  os.path.join(self.dir.path, 'basedir2'))
        os.rename(os.path.join(self.dir.path, 'basedir2', 'test2.h5'),
                  os.path.join(self.dir.path, 'basedir2', 'test2_.h5'))
        os.rename(os.path.join(self.dir.path, 'basedir2', 'subdir', 'test3.h5'),
                  os.path.join(self.dir.path, 'basedir2', 'subdir', 'test3_.h5'))
        filenames = os.path.join('basedir2', 'test1'),\
                    os.path.join('basedir2', 'test2_'),\
                    os.path.join('basedir2', 'subdir', 'test3_')
        lostlinks = [('/vdatae', '/a/vdatae', '/a/b/vdatae'),
                     tuple(),
                     ('/vdatae', '/a/vdatae', '/a/b/vdatae')]
        for filename, lost in zip(filenames, lostlinks):
            with self.h5open(filename) as root:
                data = root['/a/data']
                self.assertEqual(shape, data.shape)
                numpy.testing.assert_array_equal(datamem, flatten(data[()]))
                for path in paths:
                    vdata = root[path]
                    self.assertEqual(vshape, vdata.shape)
                    isequal = (datamem == flatten(vdata[()])).all()
                    if path in lost:
                        self.assertFalse(isequal, msg=nexus.getUri(vdata))
                    else:
                        self.assertTrue(isequal, msg=nexus.getUri(vdata))


def test_suite():
    """Test suite including all test suites"""
    testSuite = unittest.TestSuite()
    testSuite.addTest(test_nexus('test_nexus_StringAttribute'))
    testSuite.addTest(test_nexus('test_nexus_StringDataset'))
    testSuite.addTest(test_nexus('test_nexus_ExtStringAttribute'))
    testSuite.addTest(test_nexus('test_nexus_ExtStringDataset'))
    testSuite.addTest(test_nexus('test_nexus_Root'))
    testSuite.addTest(test_nexus('test_nexus_Entry'))
    testSuite.addTest(test_nexus('test_nexus_Process'))
    testSuite.addTest(test_nexus('test_nexus_Data'))
    testSuite.addTest(test_nexus('test_reshape_datasets'))
    testSuite.addTest(test_nexus('test_uri'))
    testSuite.addTest(test_nexus('test_links'))
    return testSuite


if __name__ == '__main__':
    import sys

    mysuite = test_suite()
    runner = unittest.TextTestRunner()
    if not runner.run(mysuite).wasSuccessful():
        sys.exit(1)
