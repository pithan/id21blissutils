import unittest
from testfixtures import TempDirectory
import os
import numpy
import h5py
import fabio
import itertools
try:
    import gzip
except ImportError:
    gzip = None
from fabio.edfimage import EdfImage
from .. import hdf5external


class test_nexus(unittest.TestCase):

    def setUp(self):
        self.dir = TempDirectory()
        self.scan_shape = 2, 3
        self.image_shape = 5, 4

    def tearDown(self):
        self.dir.cleanup()

    def _edf_files(self):
        nimages = numpy.product(self.scan_shape)
        shape = self.scan_shape + self.image_shape
        fshape = (numpy.product(self.scan_shape),) + self.image_shape
        fdata = numpy.arange(numpy.product(shape))
        data = fdata.reshape(shape, order='C')
        order = 'C', 'F'
        compression = 'NONE', 'GZIP'
        withindices = False, True
        options = itertools.product(order, compression, withindices)
        for i, (order, compression, withindices) in enumerate(options):
            with self.subTest(order=order, compression=compression):
                # Save as EDF:
                filename = os.path.join(self.dir.path, 'out{}.edf'.format(i))
                if order == 'F':
                    data1 = []
                    for col in numpy.transpose(data, (1, 0, 2, 3)):
                        for img in col:
                            data1.append(img)
                    data1 = numpy.array(data1)
                else:
                    data1 = data.reshape(fshape, order='C')
                edf = None
                for img in data1:
                    if compression == 'GZIP':
                        if gzip is None:
                            self.skipTest(str({'order': order,
                                               'compression': compression}))
                    #header = {'COMPRESSION': compression}
                    header = None
                    if edf is None:
                        edf = EdfImage(data=img, header=header)
                    else:
                        edf.append_frame(data=img, header=header)
                if compression == 'GZIP':
                    filename += '.gz'
                    f = gzip.GzipFile(filename, 'wb')
                else:
                    f = filename
                edf.write(f)
                # Check EDF:
                edf = fabio.open(filename)
                data2 = numpy.array([frame.data for frame in edf.frames()])
                numpy.testing.assert_array_equal(data1, data2)

                if withindices:
                    indices = list(range(nimages))
                else:
                    indices = None
                yield data, filename, indices, order, compression

    def test_edf2hdf5(self):
        for data, filename, indices, order, compression in self._edf_files():
            if indices:
                filenames = [(filename, i) for i in indices]
            else:
                filenames = filename
            # Save as HDF5:
            if compression != 'NONE':
                with self.assertRaises(RuntimeError) as context:
                    kwargs = hdf5external.add_edf_arguments(filenames)
                continue
            else:
                kwargs = hdf5external.add_edf_arguments(filenames)
                hdf5external.finalize(kwargs, shape=self.scan_shape,
                                          order=order)
            # TODO: external datasets do not support relative paths
            #kwargs['external'] = [(os.path.relpath(tpl[0], self.dir.path),) + tpl[1:]
            #                      for tpl in kwargs['external']]
            filename = os.path.join(self.dir.path, 'out.h5')
            with h5py.File(filename, mode='w') as f:
                data2 = f.create_dataset('data', **kwargs)
                # Check HDF5
                numpy.testing.assert_array_equal(data, data2[()])

    def test_append(self):
        n = 3
        scan_shape = list(self.scan_shape)
        scan_shape[-1] *= n
        scan_shape = tuple(scan_shape)
        enpoints = numpy.product(scan_shape)
        for data, filename, indices, order, compression in self._edf_files():
            if indices:
                filenames = [(filename, indices)]
            else:
                filenames = [filename]
            if compression != 'NONE':
                continue
            for shape in tuple(), scan_shape:
                kwargs1 = {}
                for i in range(n):
                    kwargs1 = hdf5external.add_edf_arguments(filenames,
                                                             createkwargs=kwargs1)
                hdf5external.finalize(kwargs1, shape=shape, order=order)
                kwargs2 = hdf5external.add_edf_arguments(filenames*n)
                hdf5external.finalize(kwargs2, shape=shape, order=order)
                self.assertEqual(kwargs1, kwargs2)
                npoints = numpy.product(kwargs1['shape']) //\
                          numpy.product(self.image_shape)
                self.assertEqual(npoints, enpoints)


def test_suite():
    """Test suite including all test suites"""
    testSuite = unittest.TestSuite()
    testSuite.addTest(test_nexus('test_edf2hdf5'))
    testSuite.addTest(test_nexus('test_append'))
    return testSuite


if __name__ == '__main__':
    import sys

    mysuite = test_suite()
    runner = unittest.TextTestRunner()
    if not runner.run(mysuite).wasSuccessful():
        sys.exit(1)
