import re
import numpy
import logging


class CustomLogger(logging.LoggerAdapter):

    def process(self, msg, kwargs):
        return '[{}] {}'.format(str(self.extra), msg), kwargs


def normalize_nexus_name(name):
    # TODO: could cause unique names to become non-unique ...
    name = re.sub('[^a-zA-Z0-9_]+', '_', name)
    #name = re.sub('_+', '_', name)
    return name


def format_bytes(size):
    # 2**10 = 1024
    power = 2**10
    n = 0
    power_labels = {0: 'B', 1: 'KB', 2: 'MB', 3: 'GB', 4: 'TB'}
    while size > power:
        size /= power
        n += 1
    return '{:.01f}{}'.format(size, power_labels[n])


def shape_to_size(shape):
    return numpy.product(shape, dtype=int)


def split_shape(shape, detector_ndim):
    scan_ndim = len(shape) - detector_ndim
    scan_shape = shape[:scan_ndim]
    detector_shape = shape[scan_ndim:]
    return scan_shape, detector_shape
